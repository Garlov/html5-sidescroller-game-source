// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// load player image
var playerImageReady = false;
var playerImage = new Image();
playerImage.onload = function () {
	playerImageReady = true;
};
playerImage.src = "images/player.png";


//load background image
var backgroundImageReady = false;
var backgroundImage = new Image();
backgroundImage.onload = function () {
	backgroundImageReady = true;
};
backgroundImage.src = "images/background.png";

// player movement speed
var playerSpeed = 256;

//player Position
var playerPosition = {
	x: 0,
	y: 0
};

// arrays of keys currently pressed
var keysPressed = {};

//listen for key press and key release
addEventListener("keydown", function (e) {
	keysPressed[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
	delete keysPressed[e.keyCode];
}, false);


// Update game objects
var update = function (modifier) {
	if (37 in keysPressed) { // Player holding left
		playerPosition.x -= playerSpeed * modifier;
		//playerImage.rotate(0.5);
	}
	if (39 in keysPressed) { // Player holding right
		playerPosition.x += playerSpeed * modifier;
	}
};

// Draw everything
var render = function () {
	//draw the background and moves it when right or left key is pressed

	if (backgroundImageReady) {		//main background
		var backgroundPosition = -playerPosition.x % canvas.width;
		ctx.drawImage(backgroundImage, backgroundPosition, 0);
		if(playerPosition.x >= 0) {	//draws the second background, creating an endless scrolling background
		
			ctx.drawImage(backgroundImage, backgroundPosition + canvas.width, 0);
		}
		else if(playerPosition.x < 0){
			ctx.drawImage(backgroundImage, backgroundPosition - canvas.width, 0);
		}
	}
	//draw player
	if (playerImageReady) {
		// save old coordinate system so we wont fuck with everything
		ctx.save(); 
		// move to the middle of where we want to draw our image
		ctx.translate(canvas.width/2, canvas.height-64);
		// rotate around that point
		ctx.rotate(0.02 * (playerPosition.x));
	 	//draw playerImage
		ctx.drawImage(playerImage, -playerImage.width/2, -playerImage.height/2);
		//and restore coordniate system to default
		ctx.restore();
	}
	 
	

	//Draws position of the player
	ctx.fillStyle = "rgb(0, 0, 0)";
	ctx.font = "16px Helvetica";
	ctx.textAlign = "left";
	ctx.textBaseline = "top";
	ctx.fillText("Player Position    X: " + playerPosition.x + " Y: " + playerPosition.y, 32, 32);

	//console.log(playerPosition.x);
};

// Game loop
var main = function () {
	var now = Date.now();
	var delta = now - then;

	update(delta / 1000);
	render();

	then = now;
};

//starts the game and execute main as fast as possible
//reset();
var then = Date.now();
setInterval(main, 1);
